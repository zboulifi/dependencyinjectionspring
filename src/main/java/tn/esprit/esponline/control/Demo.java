package tn.esprit.esponline.control;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import tn.esprit.esponline.config.BeansConfiguration;
public class Demo {
    private static final Logger logger = Logger.getLogger(Demo.class);
    private ApplicationContext applicationContext;
    public void verifyBeans () {
        logger.info("In verifyBeans() : ");
        applicationContext = new AnnotationConfigApplicationContext(BeansConfiguration.class);
        logger.debug("Contains userControlImpl " + applicationContext.containsBeanDefinition("userControlImpl"));
        logger.debug("Contains userServiceImpl " +applicationContext.containsBeanDefinition("userServiceImpl"));
        logger.debug("Contains userDAOImpl " +applicationContext.containsBeanDefinition("userDAOImpl"));
        logger.info("Out verifyBeans() : ");
    }
}

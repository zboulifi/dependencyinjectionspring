package tn.esprit.esponline.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"tn.esprit.esponline.control","tn.esprit.esponline.service", "tn.esprit.esponline.dao"})
public class BeansConfiguration {
}
